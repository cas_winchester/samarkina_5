"""
модуль, в котором определены функции, которые будут вызываться внутри
функции main(), которая будет определена в этом модуле.
для работоспособности функций импортированы модули image_class и photoalbum.
"""


import image_class as ic
import photoalbum as pa


def create_image(name: str, description='нет описания'):
    """
    функция по созданию экземпляра класса JPGImage, PNGImage или GIFImage

    Данная функция создает экземпляр класса JPGImage, PNGImage или GIFImage,
    пользователю нужно ввести название изображения, которое будет отображаться
    при вызове функции print() для данного экземпляра или при показе изображения
    в фотоальбоме, а так же выбрать расширение изображения, которое определит
    выбранный класс

    :param name: выбранное пользователем название фото, которое будет
    отображаться при строковом выводе или в фотоальбоме
    :param description: описание содержания изображения, чтобы пользователь
    включил воображение и сам его представил
    :return: созданный экземпляр

    Raises:
    TypeError

    Examples:
    >>>create_image()
    Traceback (most recent call last):
    File "C:\Users\Lost\AppData\Local\Programs\Python\Python310\lib\code.py",
    line 90, in runcode
        exec(code, self.locals)
    File "<input>", line 1, in <module>
    TypeError: create_image() missing 1 required positional argument: 'name'
    >>>create_image('name', 'description')
    Выберите расширение изображения: jpg, png, gif.
    Ваш выбор: >? gif
    Изображение под названием "name" с расширением gif. Описание изображение: description.
    <image_class.GIFImage object at 0x000001D2FFD326B0>
    >>>create_image('name')
    Выберите расширение изображения: jpg, png, gif.
    Ваш выбор: >? jpg
    Изображение под названием "name" с расширением jpg. Описание изображение: нет описания.
    <image_class.JPGImage object at 0x000001D2FFD32320>
    """

    while True:
        choice = input('Выберите расширение изображения: jpg, png, gif.\n'
                       'Ваш выбор: ')
        if choice in ['jpg', 'png', 'gif']:
            if choice == 'jpg':
                image = ic.JPGImage(name, description)
            elif choice == 'png':
                image = ic.PNGImage(name, description)
            else:
                image = ic.GIFImage(name, description)
        else:
            print('Выбрано неподходящее расширение. Попробуйте снова.')
            continue
        print(image)
        return image


def create_album(name: str, *args):
    """
    функция по созданию фотоальбома (экземпляра класса PhotoAlbum) с
    изображениями

    Функция создает экземпляр класса PhotoAlbum, пользователю нужно ввести
    название альбома, которое будет отображаться при вызове функции print()
    для данного экземпляра и (необязательно) добавить изображения - экземпляры
    классов JPGImage, PNGImage или GIFImage - как дополнительные неименнованные
    аргументы.

    :param name: название альбома
    :param args: изображения, которые будут вложены в альбом
    :return: созданный альбом

    Raises:
    TypeError

    Examples:
    >>>create_album()
    Traceback (most recent call last):
      File "C:\Users\Lost\AppData\Local\Programs\Python\Python310\lib\code.py",
      line 90, in runcode
        exec(code, self.locals)
      File "<input>", line 1, in <module>
    TypeError: create_album() missing 1 required positional argument: 'name'
    >>>create_album('name')
    Создание фотоальбома "name"...
    Альбом создан!
    Содержание фотоальбома: []
    <photoalbum.PhotoAlbum object at 0x000001D2FFD32980>
    >>>create_album('name', a)
    Создание фотоальбома "name"...
    Альбом создан!
    Содержание фотоальбома: ['изображение под названием "b", его описание: нет описания']
    <photoalbum.PhotoAlbum object at 0x000001D2FFD32620>
    """
    album = pa.PhotoAlbum(name, *args)
    print(album)
    return album


def main():
    """
    выполняет роль меню для пользователя

    Выводит на экран следующую информацию:
    Программа управления показа изображениями.
    Для упрощения работы заранее созданы 3 изображения: image_1, image_2 и
    image_3.
    Изображение под названием "image_1" с расширением jpg. Описание изображение:
    котенок лежит на диване.
    Изображение под названием "image_2" с расширением gif. Описание изображение:
    кот играется с фантиком и носится по дому.
    Изображение под названием "image_3" с расширением png. Описание изображение:
    котенок без заднего фона (для фотошопа).
    Доступные опции:
    1. Создать свое изображение.
    2. Создать фотоальбом с имеющимися тремя изображениями.
    3. Создать изображение и поместить его в альбом с тремя имеющимися
    изображениями.
    4. Создать фотоальбом с имеющимися изображениями и устроить 'слайд-шоу'.
    5. Показать изображения в альбоме по определенному индексу.
    6. Создать альбом, создать изображение и вложить изображение в готовый
    альбом.
    7. Удалить определенное изображение с альбома.
    Во избежание ошибок в работе при выборе любого пункта изображения и альбомы
    будут пересоздаваться.
    Ваш выбор:

    При вводе номера действия исполняет специальный код для него. Функция
    содержит вызовы функций create_image() и create_album().

    :return: -
    """
    print("Программа управления показа изображениями.\nДля упрощения работы "
          "заранее созданы 3 изображения: image_1, image_2 и image_3.")
    image_1 = ic.JPGImage('image_1', 'котенок лежит на диване')
    image_2 = ic.GIFImage('image_2', 'кот играется с фантиком и носится по дому')
    image_3 = ic.PNGImage('image_3', 'котенок без заднего фона (для фотошопа)')
    while True:
        print(image_1)
        print(image_2)
        print(image_3)
        print("Доступные опции:\n1. Создать свое изображение.\n2. Создать фотоальбом "
              "с имеющимися тремя изображениями.\n3. Создать изображение и "
              "поместить его в альбом с тремя имеющимися изображениями.\n"
              "4. Создать фотоальбом с имеющимися изображениями и устроить "
              "'слайд-шоу'.\n5. Показать изображения в альбоме по определенному "
              "индексу.\n6. Создать альбом, создать изображение и вложить"
              " изображение в готовый альбом.\n7. Удалить определенное изображение"
              " с альбома.\nВо избежание ошибок в работе при выборе любого пункта"
              " изображения и альбомы будут пересоздаваться.")
        try:
            choice = int(input('Ваш выбор: '))
        except ValueError:
            print('Выбор по умолчанию - 2')
            choice = 2
        if choice == 1:
            name = input('Введите название фото: ')
            if len(name) == 0:
                print('Вы ввели пустое название')
                continue
            description = input('Введите описание: ')
            create_image(name, description)
        if choice == 2:
            name = input('Введите название альбома: ')
            if len(name) == 0:
                print('Вы ввели пустое название')
                continue
            create_album(name, image_1, image_2, image_3)
        if choice == 3:
            name = input('Введите название фото: ')
            if len(name) == 0:
                print('Вы ввели пустое название')
                continue
            description = input('Введите описание: ')
            image = create_image(name, description)
            name = input('Введите название альбома: ')
            if len(name) == 0:
                print('Вы ввели пустое название.')
                continue
            create_album(name, image, image_1, image_2, image_3)
        if choice == 4:
            # циклический показ изображений
            name = input('Введите название альбома: ')
            if len(name) == 0:
                print('Вы ввели пустое название.')
                continue
            album = create_album(name, image_1, image_2, image_3)
            print('Для продолжения показа нажмите Enter, для остановки напишите'
                  ' stop после показа последнего изображения')
            while True:
                for i in album.content:
                    print(i)
                    choice = input()
                if choice == 'stop':
                    break
            print('Показ окончен.')
        if choice == 5:
            album = create_album("альбом", image_1, image_2, image_3)
            try:
                choice = int(input("Введите порядковый номер изображения, "
                                   "которое хотите увидеть. "))
            except ValueError:
                print('Вы ввели неподходящее значение.')
                continue
            try:
                print(album.content[choice-1])
            except IndexError:
                print('Неподходящий индекс')
        if choice == 6:
            name = input('Введите название альбома: ')
            if len(name) == 0:
                print('Вы ввели пустое название')
                continue
            album = create_album(name, image_1, image_2, image_3)
            name = input('Введите название фото: ')
            if len(name) == 0:
                print('Вы ввели пустое название')
                continue
            description = input('Введите описание')
            image = create_image(name, description)
            album.content.append(f'изображение под названием "{image.name}", '
                                 f'описание: {image.description}')
            print(album)
        if choice == 7:
            album = create_album("альбом", image_1, image_2, image_3)
            print('Содержание: ')
            try:
                choice = int(input("Введите порядковый номер изображения, "
                                   "которое хотите удалить. "))
            except ValueError:
                print('Вы ввели неподходящее значение.')
                continue
            try:
                del album[choice]
            except IndexError:
                print('Неподходящий индекс')
            print(f'Содержание альбома: {album.content}')
        choice = input('Продолжить работу? [да/нет]? ')
        if not choice == 'да':
            break
