# Файл README для модуля functions.py

### Какие функции реализованы в файле:

* create_image()

* create_album()

* main()


### Добавлена расширенная документация к функциям:

* [ ] create_image()

* [ ] create_album()

* [ ] main()


**_Импортированные модули:_**
```
import image_class as ic
import photoalbum as pa
```

### Пример формулы, необходимый для отчета:

$`2 + 2 = 5`$

[Больше подробностей по данному модулю:](https://vk.com/away.php?utf=1&to=https%3A%2F%2Fm.youtube.com%2Fwatch%3Fv%3DdQw4w9WgXcQ)


Спасибо, что дочитали этот бред до конца!

![meme](https://sun9-79.userapi.com/impg/m2qyXLGi6-iupD3VreUtcpePElVt2nRBUO_Wjw/BFs1NJlktzo.jpg?size=213x237&quality=95&sign=0fceaa9824c0af4b5cc9ae1b5414c574&type=album)
